
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <stdint.h>


enum opcode_decode {R = 0x33, I = 0x13, S = 0x23, L = 0x03, B = 0x63, JALR = 0x67, JAL = 0x6F, AUIPC = 0x17, LUI = 0x37};

typedef struct {
    size_t data_mem_size_;
    uint32_t regfile_[32];
    uint32_t pc_;
    uint8_t* instr_mem_;
    uint8_t* data_mem_;
} CPU;

void CPU_open_instruction_mem(CPU* cpu, const char* filename);
void CPU_load_data_mem(CPU* cpu, const char* filename);

CPU* CPU_init(const char* path_to_inst_mem, const char* path_to_data_mem) {
	CPU* cpu = (CPU*) malloc(sizeof(CPU));
	cpu->data_mem_size_ = 0x400000;
    cpu->pc_ = 0x0;
    CPU_open_instruction_mem(cpu, path_to_inst_mem);
    CPU_load_data_mem(cpu, path_to_data_mem);
    return cpu;
}

void CPU_open_instruction_mem(CPU* cpu, const char* filename) {
	uint32_t  instr_mem_size;
	FILE* input_file = fopen(filename, "r");
	if (!input_file) {
			printf("no input\n");
			exit(EXIT_FAILURE);
	}
	struct stat sb;
	if (stat(filename, &sb) == -1) {
			printf("error stat\n");
			perror("stat");
		    exit(EXIT_FAILURE);
	}
	printf("size of instruction memory: %d Byte\n\n",sb.st_size);
	instr_mem_size =  sb.st_size;
	cpu->instr_mem_ = malloc(instr_mem_size);
	fread(cpu->instr_mem_, sb.st_size, 1, input_file);
	fclose(input_file);
	return;
}

void CPU_load_data_mem(CPU* cpu, const char* filename) {
	FILE* input_file = fopen(filename, "r");
	if (!input_file) {
			printf("no input\n");
			exit(EXIT_FAILURE);
	}
	struct stat sb;
	if (stat(filename, &sb) == -1) {
			printf("error stat\n");
			perror("stat");
		    exit(EXIT_FAILURE);
	}
	//printf("read data for data memory: %d Byte\n\n",sb.st_size);

    cpu->data_mem_ = malloc(cpu->data_mem_size_);
	fread(cpu->data_mem_, sb.st_size, 1, input_file);
	fclose(input_file);
	return;
}



void lui_handler (uint32_t instruction, CPU* cpu){
	// rd after 7bit OP-code, rd 5bits max 31 = 0x1F
	uint8_t rd = (0x1F) & (instruction >> 7);
	//2^20 = 1048575 = 0xFFFFF, shift 12 bits from rd and opcode
	uint32_t imm = (0xFFFFF000) & (uint32_t)(instruction);
	//shift bits by 12bits, weil imm[12:31], first 12 bits 0
	
	//execute
	//printf("lui: rd %d, imm: %X pc: %d\n", rd, imm, cpu->pc_);
	//if(rd!= 0){
		cpu->regfile_[rd]= imm;
	//}
	
	cpu->pc_ = cpu->pc_+4;
	//printf("x%d: %X\n", rd,cpu->regfile_[rd] );
	//cpu->regfile_[0] =0;


}
void i_handler(uint32_t instruction,CPU* cpu){
//7bits opcode, 2^5 mask = 0x1F
	uint8_t rd = (uint8_t) (0x1F) & (instruction >> 7);
	//7bits opcode, 5bits rd, 2^3 bit mask = 0x7
	uint8_t func3 = (uint8_t)(0x7) & (instruction >> 12);
	// 7bit opcode, 5bit rd, 3 bit func3, 2^5 mask = 0x1F
	uint8_t rs1 = (uint8_t) (0x1F) & (instruction >> 15);
	// 7bit opcode, 5bit rd, 3 bit func3, 5bit rs1, 2^12 mask = 0xFFF
	uint32_t imm = (uint32_t) (0xFFF) & (instruction >> 20);
	uint8_t last_bit =(uint8_t) (1) & (instruction >> 31) ;
	//shamt = imm[0:4] https://fraserinnovations.com/risc-v/risc-v-instruction-set-explanation/
	uint8_t shamt = (uint8_t) (0x1F) & (instruction >> 20);
	//sign bit extension
	if(last_bit == 0){
		//printf("positive: ");
		imm = (0x0) | imm;
		
	}else{
		//printf("negative: ");
		imm = (0xFFFFF000) | imm;
	}
	// 2^7 = 127 =0x7F last 7bits, shift: 25
	uint8_t func7 =  (0x7F) & (instruction >> 25);
	
	switch (func3){
		//ADDI
		case 0:
			//printf("ADDI: rd:%d rs1:%d imm:%d pc: %d  \n", rd, rs1, imm, cpu->pc_ );
			//if(rd!= 0){
				cpu->regfile_[rd]= cpu->regfile_[rs1] + imm;
			//}
			
			cpu->pc_ = cpu ->pc_ +4;
			//printf("x%d: %X\n", rd,cpu->regfile_[rd] );
			break;
		//SLTI
		case 2:
			// 7bit opcode, 5bit rd, 3 bit func3, 5bit rs1, 2^12 mask = 0xFFF
			//printf("SLTI rd x%d, rs1 %d imm %d\n", rd, rs1, imm);
			//if(rd!= 0){
				cpu->regfile_[rd]= cpu->regfile_[rs1] < imm;
			//}
			cpu->pc_ = cpu ->pc_ +4;
			//printf("x%d: %X\n", rd,cpu->regfile_[rd] );
			break;
		//SLTIU
		case 3:
			//printf("SLTIU rd x%d, rs1 %d imm %d\n", rd, rs1, imm);
			// 7bit opcode, 5bit rd, 3 bit func3, 5bit rs1, 2^12 mask = 0xFFF
			//if(rd!= 0){
				cpu->regfile_[rd]= cpu->regfile_[rs1] < imm;
			//}
			cpu->pc_ = cpu ->pc_ +4;
			//printf("x%d: %X\n", rd,cpu->regfile_[rd] );
			break;
		//XORI '^' bitwise XOR
		case 4:
			//printf("XORI rd x%d, rs1 %d imm %d\n", rd, rs1, imm);
			// 7bit opcode, 5bit rd, 3 bit func3, 5bit rs1, 2^12 mask = 0xFFF
			//if(rd!= 0){
				cpu->regfile_[rd]= cpu->regfile_[rs1] ^ imm;
			//}
			cpu->pc_ = cpu ->pc_ +4;
			//printf("x%d: %X\n", rd,cpu->regfile_[rd] );
			break;
		//ORI 
		case 6:
			//printf("ORI rd x%d, rs1 %d imm %d\n", rd, rs1, imm);
			// 7bit opcode, 5bit rd, 3 bit func3, 5bit rs1, 2^12 mask = 0xFFF
			//if(rd!= 0){
				cpu->regfile_[rd]= cpu->regfile_[rs1] | imm;
			//}
			//cpu->regfile_[0]=0;
			cpu->pc_ = cpu ->pc_ +4;
			//printf("x%d: %X\n", rd,cpu->regfile_[rd] );
			break;
		//ANDI	
		case 7:
			//printf("ANDI rd x%d, rs1 %d imm %d\n", rd, rs1, imm);
			// 7bit opcode, 5bit rd, 3 bit func3, 5bit rs1, 2^12 mask = 0xFFF
			//if(rd!= 0){
				cpu->regfile_[rd]= cpu->regfile_[rs1] & imm;
			//}
			cpu->pc_ = cpu ->pc_ +4;
			//printf("x%d: %X\n", rd,cpu->regfile_[rd] );
			break;
		//SLLI
		case 1:
			//printf("SLLI: rd x%d, rs1 %d imm %d\n", rd, rs1, shamt);
			// https://fraserinnovations.com/risc-v/risc-v-instruction-set-explanation/
			// 7bit opcode, 5bit rd, 3 bit func3, 5bit rs1, 2^5 mask = 0x1F
			//shamt = imm[0:4] - shiftamount
			//if(rd!= 0){
				cpu->regfile_[rd]= cpu->regfile_[rs1] << shamt;
			//}
			//cpu->regfile_[0]=0;
			cpu->pc_ = cpu ->pc_ +4;
			//printf("x%d: %X\n", rd,cpu->regfile_[rd] );
			break;
		//SRLI or SRLAI
		case 5:
			// 7bit opcode, 5bit rd, 3 bit func3, 5bit rs1, 2^5 mask = 0x1F
			//SRLI
			if (func7 == 0){
				//printf("SRLI rd x%d, rs1 x%d imm %d\n", rd, rs1, shamt);
				//if(rd!= 0){
					cpu->regfile_[rd]= cpu->regfile_[rs1] >> shamt;
				//}
				//cpu->regfile_[0]=0;
				cpu->pc_ = cpu ->pc_ +4;
				//printf("x%d: %X\n", rd,cpu->regfile_[rd] );
			}
			// SRAI
			else{
				
				//printf("SRAI rd x%d, rs1 x%d imm %d\n", rd, rs1, shamt);
				//printf("Before Shift. x%d: %X\n", rd,cpu->regfile_[rs1] );
				//if(rd!= 0){
					cpu->regfile_[rd]= (int8_t)cpu->regfile_[rs1] >>(int8_t) (shamt);

				//}
				cpu->pc_ = cpu ->pc_ +4;
				//printf("After shift. x%d: %X\n", rd,cpu->regfile_[rd] );
			}
			break;
			

		
	}
	//cpu->regfile_[0] =0;
}

void r_handler(uint32_t instruction,CPU* cpu){
	//7bits opcode 5bit mask rd
	uint8_t rd= (0x1F) & (uint8_t) (instruction >> 7);
	//printf("rd: %d", rd);
	//7bits opcode, 5bits rd, 2^3 bit mask = 0x7
	uint8_t func3 = (0x7) & (instruction >> 12);
	//7 bit opcode, 5bits rd, 3bits func3, mask 2^5 = 0x1F totalshift: 15bit
	uint8_t rs1 = (0x1F) & (uint8_t) (instruction >> 15);
	//printf(" rs1: %d", rs1);
	//7 bit opcode, 5bits rd, 3bits func3, 5bits rs1, mask 2^5 = 0x1F totalshift: 20bit
	uint8_t rs2= (0x1F) & (uint8_t) (instruction >> 20);
	//printf(" rs2: %d \n", rs2);
	//7 bit opcode, 5bits rd, 3bits func3, 5bits rs1,5bits rs2, mask 2^7 = 0x7F totalshift: 25bit
	uint8_t func7= (0x7F) & (uint8_t) (instruction >> 25);
	//printf("func3: %d func7: %d :rd x%d rs1 x%d rs2 x%d \n ", func3, func7, rd, rs1, rs2);
	switch(func3){
		// ADD or SUB
		
		case 0:
			//SUB
			
			switch(func7){
				case(0x20):
					//printf("SUB: x%d x%d x%d \n ", rd, rs1, rs2);
					//if(rd!= 0){
						cpu->regfile_[rd]=cpu->regfile_[rs1] - cpu->regfile_[rs2];
					//}
					//cpu->regfile_[0]=0;
					
					cpu->pc_ = cpu ->pc_ + 4;
					//printf("x%d: %X\n", rd,cpu->regfile_[rd] );
					break;
				case(0):
					
					//if(rd!= 0){
						cpu->regfile_[rd]=cpu->regfile_[rs1] + cpu->regfile_[rs2];
					//}
					//cpu->regfile_[0]=0;
					cpu->pc_ = cpu ->pc_ + 4;
					//printf("x%d: %X\n", rd,cpu->regfile_[rd] );
					break;
			}
			
			break;
		//SLL	
		case 1:
			//SLL
			
			//if(rd!= 0){
				cpu->regfile_[rd]=cpu->regfile_[rs1] << cpu->regfile_[rs2];
			//}
			cpu->pc_ = cpu ->pc_ + 4;
			//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			break;
		//SLT
		case 2:
			//SLT
			
			//if(rd!= 0){
				cpu->regfile_[rd] = (int32_t) cpu->regfile_[rs1] < (int32_t)  cpu->regfile_[rs2];
			//}
			cpu->regfile_[0]=0;
			cpu->pc_ = cpu ->pc_ + 4;
			//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			break;
			
		//SLTU
		case 3:
			//SLTU
			
			//if(rd!= 0){
				cpu->regfile_[rd] = cpu->regfile_[rs1] < cpu->regfile_[rs2];
			//}
			cpu->pc_ = cpu ->pc_ + 4;
			//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			break;
			
		//XOR
		case 4:
			//XOR
			
			//if(rd!= 0){
				cpu->regfile_[rd]=cpu->regfile_[rs1] ^ cpu->regfile_[rs2];
			//}
			//cpu->regfile_[0]=0;
			cpu->pc_ = cpu ->pc_ + 4;
			//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			break;
			
		//SRL or SRA
		case 5:
			//SRL
			if(func7 == 0x00){
				
				//if(rd!= 0){
					cpu->regfile_[rd] = cpu->regfile_[rs1] >> cpu->regfile_[rs2];
				//}
				//cpu->regfile_[0]=0;
				cpu->pc_ = cpu ->pc_ + 4;
				
				//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			}
			//SRA
			if(func7 == 0x20){
				
				//if(rd!= 0){
				cpu->regfile_[rd] = (int32_t)cpu->regfile_[rs1] >> cpu->regfile_[rs2];
				//cpu->regfile_[0]=0;
				//}
				cpu->pc_ = cpu ->pc_ + 4;
				
				//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			} 
			break;
		//OR
		case 6:
			//OR
			
			//if(rd!= 0){
				cpu->regfile_[rd] = cpu->regfile_[rs1] | cpu->regfile_[rs2];
			//}
			//cpu->regfile_[0]=0;
			cpu->pc_ = cpu ->pc_ + 4;
			//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			break;
		//AND
		case 7:
			//AND
			
			//if(rd!= 0){
				cpu->regfile_[rd] = cpu->regfile_[rs1] & cpu->regfile_[rs2];
			//}
			cpu->regfile_[0]=0;
			cpu->pc_ = cpu ->pc_ + 4;
			//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			break;
			
			//Error
	printf("command cannot resolve \n");
	}
	
	//cpu->regfile_[0] =0;
}

void s_handler(uint32_t instruction,CPU* cpu){
	//7bits opcode, 2^5 mask = 0x1F
	uint8_t imm_0_4 = (uint8_t) (0x1F) & (instruction >> 7);
	//7bits opcode, 5bits imm[0:4], 2^3 bit mask = 0x7, 12 bit shift
	uint8_t func3 = (0x7) & (instruction >> 12);
	//7bits opcode, 5bits imm[0:4], 3bit func3, totalshift:15 2^5 mask = 0x1F 15 bit shift
	uint8_t rs1 = (uint8_t) (0x1F) & (instruction >> 15);
	//7bits opcode, 5bits imm[0:4], 3bit func3, 5 bits rs1, totalshift:20 2^5 mask = 0x1F 20bit shift
	uint8_t rs2 = (uint8_t) (0x1F) & (instruction >> 20);
	//7bits opcode, 5bits imm[0:4], 3bit func3, 5 bits rs1, 5bits rs2 totalshift:25 2^7 mask = 127 = 0x7F 25bit shift
	uint8_t imm_5_11 = (uint8_t) (0x7F) & (instruction >> 25);
	//correct imm by shifting
	uint32_t imm_s = (uint32_t) (imm_5_11<< 5) | (imm_0_4 );
	//printf("instruction 0x%X\n", instruction);
	uint8_t last_bit =(uint8_t) (1) & (imm_5_11 >> 6) ;

	//sign bit extension
	if(last_bit == 1){
		//printf("negative: ");
		imm_s = (0xFFFFF000) | imm_s;
		
	}
	//printf("s-handler imm: %d\n", imm);
	switch (func3){
	//SB
	case 0:
		
		if((cpu->regfile_[rs1] + imm_s) == 0x5000){


			putchar(cpu->regfile_[rs2]);
		}else{
			cpu->data_mem_[cpu->regfile_[rs1] + (int32_t)imm_s] = (uint8_t) cpu->regfile_[rs2];
		}
		cpu->pc_ = cpu->pc_ + 4;
		
		
		break;
	//SH	
	case 1:
		*(uint16_t*)(cpu->data_mem_ + cpu->regfile_[rs1]+imm_s)= (uint16_t)(cpu->regfile_[rs2]);
		//cpu->regfile_[0]=0;
		cpu->pc_ = cpu->pc_ + 4;
		break;
	//SW
	case 2:
		*(uint32_t*)(cpu->data_mem_ + cpu->regfile_[rs1]+ (int32_t)imm_s)= (uint32_t)(cpu->regfile_[rs2]);
		//cpu->regfile_[0]=0;
		cpu->pc_ = cpu->pc_ + 4;
		break;

	default:
		break;
	}
	//cpu->regfile_[0] =0;
}

void l_handler(uint32_t instruction,CPU* cpu){
	//7bits opcode 5bit mask rd
	uint8_t rd= (0x1F) & (uint8_t) (instruction >> 7);
	//7bits opcode, 5bits rd, 2^3 bit mask = 0x7
	uint8_t func3 = (0x7) & (instruction >> 12);
	//7 bit opcode, 5bits rd, 3bits func3, mask 2^5 = 0x1F totalshift: 15bit
	uint8_t rs1= (0x1F) & (uint8_t) (instruction >> 15);
	//7 bit opcode, 5bits rd, 3bits func3,5 bits rs1, mask 2^12 = 4095 = 0xFFF totalshift: 15bit
	int16_t imm = (0xFFF) & (int16_t) (instruction >> 20);
	uint8_t last_bit =(uint8_t) (1) & (instruction >> 31) ;
	
	if(last_bit == 1){
		//printf("negative: ");
		imm = (0xFFFFF000) | imm;
		
	}
	
	int32_t data_ext = 0;
	switch (func3){	
		
		//LB
		case 0:
			
			//printf("LB: x%d x%d imm:%d \n", rd, rs1, imm);
			//if(rd!= 0){
			//int32_t data_ext = 0;
			data_ext = (cpu->data_mem_[cpu->regfile_[rs1]+ imm]);
			if(1 ==(data_ext >> 7)){
					//printf("Before Extension: %d\n", data_ext);
					data_ext = (0xFFFFFF00 | data_ext);
					//printf("After Extension: %d\n", data_ext);

			}
			//printf("%d\n",data_ext );
			cpu->regfile_[rd] = data_ext;
			//cpu->regfile_[0]=0;
			//}
			cpu->pc_ = cpu->pc_ + 4;
			//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			break;
		//LH - Vorzeichen Erweiterung fehlt
		case 1:
			//printf("LH: x%d x%d imm:%d \n", rd, rs1, imm);
			//if(rd!= 0){
				//int32_t data_ext = 0;
				data_ext = *(uint16_t*)(cpu->regfile_[rs1] + imm + cpu->data_mem_);
				if(1 ==(data_ext >> 15)){
					//printf("Before Extension: %d\n", data_ext);
					data_ext = (0xFFFF0000 | data_ext);
					//printf("After Extension: %d\n", data_ext);

				}
				//printf("%d\n",data_ext );
				cpu->regfile_[rd] = data_ext;
			//}
			//cpu->regfile_[0]=0;
			cpu->pc_ = cpu->pc_ + 4;
			//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			break;
		//LW
		case 2:
			//printf("LW: x%d x%d imm:%d \n", rd, rs1, imm);
			//if(rd!= 0){
				cpu->regfile_[rd] = *(uint32_t*)(cpu->regfile_[rs1] + imm + cpu->data_mem_);
			//}
			//cpu->regfile_[0]=0;
			cpu->pc_ = cpu->pc_ + 4;
			//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			break;
		//LBU
		case 4:
			//printf("LBU: x%d x%d imm:%d \n", rd, rs1, imm);
			//if(rd!= 0){
				cpu->regfile_[rd] = cpu->data_mem_[cpu->regfile_[rs1]+ imm];
			//}
			
			cpu->pc_ = cpu->pc_ + 4;
			//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			break;
		//LHU	
		case 5:
			//printf("LHU: x%d x%d imm:%d \n", rd, rs1, imm);
			//if(rd!= 0){
				cpu->regfile_[rd] =  *(uint16_t*)(cpu->regfile_[rs1] + imm + cpu->data_mem_);
			//}
			cpu->pc_ = cpu->pc_ + 4;
			//printf("x%d %X\n", rd, cpu->regfile_[rd]);
			break;
	
		default:
		printf("Error: Command not found");
			break;
	}
	
	//cpu->regfile_[0] =0;
}


void b_handler(uint32_t instruction,CPU* cpu ){
	// 7bit shift OPCODE, mask 5 bit: 2^1 = 0x1F
	uint32_t imm_11= (0x1) &  (instruction >> 7);
	// 7bit op code, 1bit imm[11], 4 bit mask = 15 = 0xF
	uint32_t imm_1_4 = (0xF) &  (instruction >> 8);
	// 7bit opcode, 5 bits imm, 3bit mask = 2^3 = 7 = 0x7, shifttotal:12
	uint8_t funct3 = (0x7) & (uint8_t) ( instruction >> 12);
	// 7bit opcode, 5bit imm, 3bit func3, mask = 2^5 = 0x1F totalshift: 15
	uint8_t rs1 = (0x1F) & (uint8_t) (instruction >> 15);
	// 7bit opcode, 5bit imm, 3bit func3, 5bit rs1, mask 2^5, totalshift: 20
	uint8_t rs2 = (0x1F) & (uint8_t) (instruction >> 20);
	// 7bit opcode, 5bit imm, 3bit func3, 5bit rs1, 5bit rs1, mask 2^6 = 0x3F totalshift: 25 
	uint32_t imm_5_10 = (0x3F) & (instruction >> 25);
	// 7bit opcode, 5bit imm, 3bit func3, 5bit rs1, 5bit rs1, 6 bit imm[5:10]. mask 2^1 = 0x1 totalshift: 31
	uint32_t imm_12 = (0x1) & (uint8_t) (instruction >> 31);

	// shift 5 imm1 concatenate imm2 
	uint8_t last_bit =(uint8_t) (1) & (instruction >> 31) ;
	int32_t imm_b = ((imm_12 << 12)| (imm_11 << 11) | (imm_5_10 << 5) | (imm_1_4 << 1)| 0);


	// add imm[1:4]
	
	if(last_bit == 1){
		//printf("positive: ");
		imm_b = (0xFFFFE000) | imm_b;
		
	}

	switch (funct3){
		//BEQ
		case 0:	
			//printf("BEQ: x%d x%d imm:%d \n", rs1, rs2, imm_b);		
			if(cpu->regfile_[rs1] == cpu->regfile_[rs2]){
				
				cpu->pc_ = cpu->pc_ + (int32_t)(imm_b);
			}else{
				
				cpu->pc_ = cpu->pc_ + 4;
			};
			
			//printf("func3 = 0");
			break;
		//BNE	
		case 1:
			//printf("BNE: x%d x%d imm:%d \n", rs1, rs2, imm_b);
			if(cpu->regfile_[rs1] != cpu->regfile_[rs2]){
				cpu->pc_ = cpu->pc_ + (int32_t)(imm_b);
			}else{
				cpu->pc_ = cpu->pc_ + 4;
			}
			break;
		//BLT
		case 4:
			//printf("BLT: x%d x%d imm:%d \n", rs1, rs2, imm_b);
			if(cpu->regfile_[rs1] < cpu->regfile_[rs2]){
				cpu->pc_ = cpu->pc_ + (int32_t)(imm_b);
				
			}else{
				cpu->pc_ = cpu->pc_ + 4;
				
			}
			break;
		//BGE
		case 5:
			//printf("BGE: x%d x%d imm:%d \n", rs1, rs2, imm_b);
			if((int32_t)cpu->regfile_[rs1] >= (int32_t)cpu->regfile_[rs2]){
				cpu->pc_ = cpu->pc_ + (int32_t)(imm_b);
				
			}else{
				cpu->pc_ = cpu->pc_ + 4;
				
			}
			break;
		//BLTU
		case 6:
			//printf("BLTU: x%d x%d imm:%d \n", rs1, rs2, imm_b);
			if(cpu->regfile_[rs1] < cpu->regfile_[rs2]){
				cpu->pc_ = cpu->pc_ + (int32_t)(imm_b);
			}else{
				cpu->pc_ = cpu->pc_ + 4;
			}
			break;
		//BGEU
		case 7:
			//printf("BGEU: x%d x%d imm:%d \n", rs1, rs2, imm_b);
			if(cpu->regfile_[rs1] >= cpu->regfile_[rs2]){
				cpu->pc_ = cpu->pc_ + (int32_t)(imm_b);
				
			}else{
				cpu->pc_ = cpu->pc_ + 4;
			}
			break;
		

	}
	

}

void auipc_handler (uint32_t instruction, CPU* cpu){
	// rd after 7bit OP-code, rd 5bits max 31 = 0x1F
	uint8_t rd = (0x1F) & (uint8_t)(instruction >> 7);
	//2^20 = 1048575 = 0xFFFFF, shift 12 bits from rd and opcode
	uint32_t imm = (0xFFFFF) & (uint32_t)(instruction >> 12);
	//shift bits by 12bits, weil imm[12:31]
	//printf("rd: %d, instruction: %d, imm: %d\n",rd, instruction, imm);
	//printf("auipc unshifted imm %X\n", imm);
	imm = 0 | (imm << 12) ;
	//printf("auipc: rd %d, imm: %X pc: %d\n ", rd, imm, cpu->pc_);
	//if(rd!= 0){
		cpu->regfile_[rd]= cpu->pc_ + imm;
	//}
	//printf("x%d %X\n", rd, cpu->regfile_[rd]);
	cpu->pc_ = cpu->pc_+4;
	//cpu->regfile_[0] =0;
}

void jal_handler (uint32_t instruction, CPU* cpu){
	// rd after 7bit OP-code, rd 5bits max 31 = 0x1F
	uint8_t rd = (0x1F) & (uint8_t)(instruction >> 7);
	//2^8 = 255 = 0xFF, shift 12 bits from rd and opcode
	uint32_t imm_12_19 = (0xFF) & (uint32_t)(instruction >> 12);
	//7 bit opcode, 5bit rd, 8 bit imm12-19, 1 bit mask = 0x1
	uint32_t imm_11 = (0x1) & (uint32_t)(instruction >> 20);
	//7bit opcode, 5bit rd, 8 bit imm[12:19], 1bit imm[11], mask 2^10 = 1023 = 0x3FF
	uint32_t imm_1_10 = (0x3FF) & (uint32_t)(instruction >> 21);
	//7bit opcode, 5bit rd, 8 bit imm[12:19], 1bit imm[11], 10 bit imm[1:10], 1bit mask = 0x1 
	uint32_t imm_20 = (0x1) & (uint32_t)(instruction >> 31);

	uint32_t imm = (imm_20 << 20) | (imm_12_19<< 12)|(imm_11 << 11) | (imm_1_10  << 1) | 0;


	if(imm_20==1){
		
		imm = (0xFFF00000) | imm;
		
	}
	//if(rd!= 0){
		cpu->regfile_[rd] = cpu->pc_ +4;
	//}
	cpu->pc_ = cpu->pc_ + (int32_t) imm;
	
}

void jalr_handler (uint32_t instruction, CPU* cpu){
	// rd after 7bit OP-code, rd 5bits max 31 = 0x1F
	uint8_t rd = (0x1F) & (uint8_t)(instruction >> 7);
	//shift 15, 7bits opcode, 5bits rd, 000 filling 3 bits, 
	uint8_t rs1 = (0x1F) & (uint32_t)(instruction >> 15);
	//7bits opcode, 5bits rd, 000 filling 3 bits, 5bits rs1, 20bits total shift, 2^12 = 4095 = 0xFFF
	uint32_t imm = (0xFFF) & (uint32_t)(instruction >> 20);
	//printf("jalr imm: %d ", imm);
	imm = imm << 12;
	uint8_t last_bit =(uint8_t) (1) & (instruction >> 31) ;
	if(last_bit == 1){
		//printf("positive: ");
		imm = (0xFFFFF000) | imm;
		
	}
	//if(rd!= 0){
		cpu->regfile_[rd]= cpu->pc_ + 4;
	//}
	cpu->pc_ = (cpu ->regfile_[rs1]) + ((int32_t)imm);
	
}

/**
 * Instruction fetch Instruction decode, Execute, Memory access, Write back
 */
void CPU_execute(CPU* cpu) {

	uint32_t instruction = *(uint32_t*)(cpu->instr_mem_ + (cpu->pc_  & 0xFFFFF));
	//printf("instruction %d\n", instruction);
	uint8_t opcode = (uint8_t)(0x7F &  instruction);
	//printf("%X\n", instruction);
	//printf("opcode %d\n", opcode);

	switch(opcode){
		case 3:
			//printf("Need L-Handler. \n");
			l_handler(instruction,cpu);
			break;
		case 19:
			//printf("Need I-Handler. \n");
			i_handler(instruction,cpu);
			break;
		case 23:
			//printf("Need AUIPC-Handler. \n");
			auipc_handler(instruction,cpu);
			break;
		case 35:
			//printf("Need S-Handler. \n");
			s_handler(instruction, cpu);
			break;
		case 51:
			//printf("Need R-Handler. \n");
			r_handler(instruction, cpu);
			break;
		case 55:
			//printf("Need LUI-Handler. - implemented \n");
			lui_handler(instruction,cpu);
			break;
		case 99:
			//printf("Need B-Handler. \n");
			b_handler(instruction,cpu);
			break;
		case 103:
			//printf("Need JALR-Handler. \n");
			jalr_handler(instruction,cpu);
			break;
		case 111:
			//printf("Need JAL-Handler. \n");
			jal_handler(instruction,cpu);
			break;
	}
	cpu->regfile_[0]=0;


}

int main(int argc, char* argv[]) {
	printf("C Praktikum\nHU Risc-V  Emulator 2022\n");

	CPU* cpu_inst;

	cpu_inst = CPU_init(argv[1], argv[2]);
	//1000000
    for(uint32_t i = 0; i < 1000000; i++) { // run 70000 cycles
    	CPU_execute(cpu_inst);
		
    }

	printf("\n-----------------------RISC-V program terminate------------------------\nRegfile values:\n");

	//output Regfile
	for(uint32_t i = 0; i <= 31; i++) {
    	printf("%d: %X\n",i,cpu_inst->regfile_[i]);
    }
    fflush(stdout);

	return 0;
}
