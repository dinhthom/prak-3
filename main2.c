
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <stdint.h>


enum opcode_decode {R = 0x33, I = 0x13, S = 0x23, L = 0x03, B = 0x63, JALR = 0x67, JAL = 0x6F, AUIPC = 0x17, LUI = 0x37};

typedef struct {
    size_t data_mem_size_;
    uint32_t regfile_[32];
    uint32_t pc_;
    uint8_t* instr_mem_;
    uint8_t* data_mem_;
} CPU;

void CPU_open_instruction_mem(CPU* cpu, const char* filename);
void CPU_load_data_mem(CPU* cpu, const char* filename);

CPU* CPU_init(const char* path_to_inst_mem, const char* path_to_data_mem) {
	CPU* cpu = (CPU*) malloc(sizeof(CPU));
	cpu->data_mem_size_ = 0x400000;
    cpu->pc_ = 0x0;
    CPU_open_instruction_mem(cpu, path_to_inst_mem);
    CPU_load_data_mem(cpu, path_to_data_mem);
    return cpu;
}

void CPU_open_instruction_mem(CPU* cpu, const char* filename) {
	uint32_t  instr_mem_size;
	FILE* input_file = fopen(filename, "r");
	if (!input_file) {
			printf("no input\n");
			exit(EXIT_FAILURE);
	}
	struct stat sb;
	if (stat(filename, &sb) == -1) {
			printf("error stat\n");
			perror("stat");
		    exit(EXIT_FAILURE);
	}
	printf("size of instruction memory: %d Byte\n\n",sb.st_size);
	instr_mem_size =  sb.st_size;
	cpu->instr_mem_ = malloc(instr_mem_size);
	fread(cpu->instr_mem_, sb.st_size, 1, input_file);
	fclose(input_file);
	return;
}

void CPU_load_data_mem(CPU* cpu, const char* filename) {
	FILE* input_file = fopen(filename, "r");
	if (!input_file) {
			printf("no input\n");
			exit(EXIT_FAILURE);
	}
	struct stat sb;
	if (stat(filename, &sb) == -1) {
			printf("error stat\n");
			perror("stat");
		    exit(EXIT_FAILURE);
	}
	//printf("read data for data memory: %d Byte\n\n",sb.st_size);

    cpu->data_mem_ = malloc(cpu->data_mem_size_);
	fread(cpu->data_mem_, sb.st_size, 1, input_file);
	fclose(input_file);
	return;
}

//////////////////////extract - Information /////////////////////////////////////////
uint8_t extract_rd (uint32_t instruction){
    uint8_t rd = (0x1F) & (instruction >> 7);
    return rd;
}

uint8_t extract_rs1 (uint32_t instruction){
    uint8_t rs1 = (uint8_t) ((0x1F) & (instruction >> 15));
    return rs1;
}


uint8_t extract_rs2 (uint32_t instruction){
    uint8_t rs2 =(uint8_t)( (0x1F) &  (instruction >> 20));
    return rs2;
}

uint8_t extract_func3(uint32_t instruction){
    uint8_t func3 = (uint8_t)((0x7) & (instruction >> 12));
    return func3;
}


uint8_t extract_shamt(uint32_t instruction){
    uint8_t shamt = (uint8_t) (0x1F) & (instruction >> 20);
    return shamt;
}


uint8_t extract_func7(uint32_t instruction){
    uint8_t func7 =  (0x7F) & (instruction >> 25);
    return func7;
}
/////////////////////////////Extract-Immediates///////////////////////////
uint32_t extract_i_imm(uint32_t instruction){
    uint32_t imm = (uint32_t) (0xFFF) & (instruction >> 20);
    uint8_t last_bit =(uint8_t) (1) & (instruction >> 31) ;
    if(last_bit == 1){
		imm = (0xFFFFF000) | imm;
	}
    return imm;
}

uint32_t extract_lui_imm(uint32_t instruction){
    uint32_t imm = (0xFFFFF000) & (uint32_t)(instruction);
    return imm;
}

uint32_t extract_s_imm(uint32_t instruction){
    uint8_t imm_0_4 = (uint8_t) (0x1F) & (instruction >> 7);
    uint8_t imm_5_11 = (uint8_t) (0x7F) & (instruction >> 25);
    uint8_t last_bit =(uint8_t) (1) & (imm_5_11 >> 6);
    uint32_t imm_s = (uint32_t) (imm_5_11<< 5) | (imm_0_4 );
    if(last_bit == 1){
		//printf("negative: ");
		imm_s = (0xFFFFF000) | imm_s;
		
	}
    return imm_s;
}

uint32_t extract_l_imm(uint32_t instruction){
    int32_t imm = (int16_t) ((0xFFF) & (instruction >> 20));
    uint8_t last_bit =(uint8_t) (1) & (instruction >> 31);
    if(last_bit == 1){
		//printf("negative: ");
		imm = (0xFFFFF000) | imm;
		
	}
	return imm;
}

int32_t extract_b_imm(uint32_t instruction){
    uint32_t imm_11= (0x1) &  (instruction >> 7);
    uint32_t imm_1_4 = (0xF) &  (instruction >> 8);
    uint32_t imm_5_10 = (0x3F) & (instruction >> 25);
    uint32_t imm_12 = (0x1) & (uint8_t) (instruction >> 31);

    uint8_t last_bit =(uint8_t) (1) & (instruction >> 31) ;
	int32_t imm_b = ((imm_12 << 12)| (imm_11 << 11) | (imm_5_10 << 5) | (imm_1_4 << 1)| 0);
    if(last_bit == 1){
		imm_b = (0xFFFFE000) | imm_b;
		
	}
    return imm_b;
}

uint32_t extract_auipc_imm(uint32_t instruction){
    uint32_t imm = (0xFFFFF) & (uint32_t)(instruction >> 12);
    imm = 0 | (imm << 12); 
    return imm;
}

uint32_t extract_jal_imm(uint32_t instruction){
    uint32_t imm_12_19 = (0xFF) & (uint32_t)(instruction >> 12);
    uint32_t imm_11 = (0x1) & (uint32_t)(instruction >> 20);
    uint32_t imm_1_10 = (0x3FF) & (uint32_t)(instruction >> 21);
    uint32_t imm_20 = (0x1) & (uint32_t)(instruction >> 31);
    uint32_t imm = (imm_20 << 20) | (imm_12_19<< 12)|(imm_11 << 11) | (imm_1_10  << 1) | 0;
    if(imm_20==1){
		
		imm = (0xFFF00000) | imm;
		
	}
    return imm;
}

uint32_t extract_jalr_imm(uint32_t instruction){
    uint32_t imm = (0xFFF) & (uint32_t)(instruction >> 20);
    imm = imm << 12;
    uint8_t last_bit =(uint8_t) (1) & (instruction >> 31) ;
	if(last_bit == 1){
		//printf("positive: ");
		imm = (0xFFFFF000) | imm;		
	}
    return imm;

}

/////////////////////Problem - Solving Methods /////////////////////////////////////
void lui_handler(uint32_t instruction, CPU* cpu){
    uint8_t rd = extract_rd(instruction);
    uint32_t imm = extract_lui_imm(instruction);
    cpu->regfile_[rd]= imm;
    cpu->pc_ = cpu->pc_+4;

}

void l_handler(uint32_t instruction, CPU* cpu){
    uint8_t rd = extract_rd(instruction);
    uint8_t func3 = extract_func3(instruction);
    uint8_t rs1 = extract_rs1(instruction);
    uint32_t imm = extract_l_imm(instruction);
    int32_t data_ext = 0;
    switch(func3){
        //LB
        case 0:
            data_ext = (cpu->data_mem_[cpu->regfile_[rs1]+ imm]);
			if(1 ==(data_ext >> 7)){
					data_ext = (0xFFFFFF00 | data_ext);       
			}
            cpu->regfile_[rd] = data_ext;
            break;
        //LH
        case 1:
            data_ext = *(uint16_t*)(cpu->regfile_[rs1] + imm + cpu->data_mem_);
		    if(1 ==(data_ext >> 15)){
					data_ext = (0xFFFF0000 | data_ext);
		    }
            cpu->regfile_[rd] = data_ext;
		    break;
        //LW
        case 2: 
            cpu->regfile_[rd] = *(uint32_t*)(cpu->regfile_[rs1] + imm + cpu->data_mem_);
            break;
        //LBU
		case 4:
            cpu->regfile_[rd] = cpu->data_mem_[cpu->regfile_[rs1]+ imm];
            break;
        //LHU
        case 5:
            cpu->regfile_[rd] =  *(uint16_t*)(cpu->regfile_[rs1] + imm + cpu->data_mem_);
            break;
        default:
		    printf("Error: Command not found");
			break;
    }
    cpu->pc_ = cpu->pc_+4;
}

void i_handler(uint32_t instruction, CPU* cpu){
    uint8_t rd = extract_rd(instruction);
    uint8_t func3 = extract_func3(instruction);
    uint8_t rs1 = extract_rs1(instruction);
    uint32_t imm = extract_i_imm(instruction);
    uint8_t shamt = extract_shamt(instruction);
    uint8_t func7 = extract_func7(instruction);


	
		switch(func3){
            //ADDI
			case 0:
				// execute addi
				cpu->regfile_[rd] = cpu->regfile_[rs1] + imm;
				break;
            //SLTI
			case 2:
				cpu->regfile_[rd] = cpu->regfile_[rs1] < imm;
				break;
            //SLTIU
			case 3:
				cpu->regfile_[rd] = cpu->regfile_[rs1] < imm;
				break;
            //XORI
			case 4:
				cpu->regfile_[rd] = cpu->regfile_[rs1] ^ imm;
				break;
            //ORI
			case 6:
				cpu->regfile_[rd] = cpu->regfile_[rs1] | imm;
				break;
            //ANDI
			case 7:
				cpu->regfile_[rd] = cpu->regfile_[rs1] & imm;
				break;
			//SLLI
			case 1:
				cpu->regfile_[rd] = cpu->regfile_[rs1] << shamt;
				break;
            //SRLI
			case 5:
                //SRLI
				if(func7 == 0){
					
					cpu->regfile_[rd] = cpu->regfile_[rs1] >> shamt;
				}else{
                //SRAI
					cpu->regfile_[rd] = (int8_t)(cpu->regfile_[rs1] >> (int8_t)shamt);
				}
		}
	// }
	cpu->pc_ = cpu->pc_ + 4;
	return;
        
}

void auipc_handler(uint32_t instruction, CPU* cpu){
    uint8_t rd = extract_rd(instruction);
    uint32_t imm = extract_auipc_imm(instruction);
    cpu->regfile_[rd]= cpu->pc_ + imm;
    cpu->pc_ = cpu->pc_+4;
}

void s_handler(uint32_t instruction, CPU* cpu){
    uint8_t func3 = extract_func3(instruction);
    uint8_t rs1 = extract_rs1(instruction);
    uint8_t rs2 = extract_rs2(instruction);
    uint32_t imm = extract_s_imm(instruction);
    switch (func3){
	//SB
	case 0:	
		if((cpu->regfile_[rs1] + imm) == 0x5000){
			putchar(cpu->regfile_[rs2]);
		}else{
			cpu->data_mem_[(cpu->regfile_[rs1] + ((int32_t)imm))] = ((uint8_t) cpu->regfile_[rs2]);
		}
		cpu->pc_ = cpu->pc_ + 4;
		break;
	//SH	
	case 1:
		(*(uint16_t*)(cpu->data_mem_ + cpu->regfile_[rs1]+imm))= ((uint16_t)(cpu->regfile_[rs2]));
		cpu->pc_ = cpu->pc_ + 4;
		break;
	//SW
	case 2:
		(*(uint32_t*)(cpu->data_mem_ + cpu->regfile_[rs1]+ (int32_t)imm))= ((uint32_t)(cpu->regfile_[rs2]));
		cpu->pc_ = cpu->pc_ + 4;
		break;
	default:
		break;
	}
}

void r_handler(uint32_t instruction, CPU* cpu){
    uint8_t rd = extract_rd(instruction);
    uint8_t func3 = extract_func3(instruction);
    uint8_t rs1= extract_rs1(instruction);
    uint8_t rs2= extract_rs2(instruction);
    uint8_t func7 = extract_func7(instruction);
    switch(func3){
		// ADD or SUB
		
		case 0:
			//SUB
			switch(func7){
				case(0x20):					
					cpu->regfile_[rd]= cpu->regfile_[rs1] - cpu->regfile_[rs2];
					cpu->pc_ = cpu ->pc_ + 4;
					break;
            //ADD
				case(0):
					cpu->regfile_[rd]= cpu->regfile_[rs1] + cpu->regfile_[rs2];
					cpu->pc_ = cpu ->pc_ + 4;
					break;
			}
			break;
		//SLL	
		case 1:
		    cpu->regfile_[rd]=cpu->regfile_[rs1] << cpu->regfile_[rs2];
			cpu->pc_ = cpu ->pc_ + 4;
			break;
		//SLT
		case 2:
			//SLT
			cpu->regfile_[rd] = ((int32_t) cpu->regfile_[rs1]) < ((int32_t)  cpu->regfile_[rs2]);
			cpu->pc_ = cpu ->pc_ + 4;
			break;
			
		//SLTU
		case 3:
			cpu->regfile_[rd] = (cpu->regfile_[rs1] < cpu->regfile_[rs2]);
			cpu->pc_ = cpu ->pc_ + 4;
			break;
			
		//XOR
		case 4:
			cpu->regfile_[rd] = cpu->regfile_[rs1] ^ cpu->regfile_[rs2];
			cpu->pc_ = cpu ->pc_ + 4;
			break;	
		//SRL or SRA
		case 5:
			//SRL
			if(func7 == 0x00){
				cpu->regfile_[rd] = (cpu->regfile_[rs1] >> cpu->regfile_[rs2]);
				cpu->pc_ = cpu ->pc_ + 4;
			}
			//SRA
			if(func7 == 0x20){
				cpu->regfile_[rd] = (((int32_t)cpu->regfile_[rs1]) >> (cpu->regfile_[rs2]));
				cpu->pc_ = cpu ->pc_ + 4;
			} 
			break;
		//OR
		case 6:
			cpu->regfile_[rd] = ((cpu->regfile_[rs1]) | (cpu->regfile_[rs2]));
			cpu->pc_ = cpu ->pc_ + 4;
			break;
		//AND
		case 7:
			//AND
		    cpu->regfile_[rd] = ((cpu->regfile_[rs1]) & (cpu->regfile_[rs2]));
			cpu->pc_ = cpu ->pc_ + 4;
			break;
			
			//Error
	printf("command cannot resolve \n");
	}

}

void b_handler(uint32_t instruction, CPU* cpu){
    int32_t imm = extract_b_imm(instruction);
    uint8_t func3 = extract_func3(instruction);
    uint8_t rs1 = extract_rs1(instruction);
    uint8_t rs2 = extract_rs2(instruction);

    switch (func3){
		//BEQ
		case 0:						
			if(cpu->regfile_[rs1] == cpu->regfile_[rs2]){
				cpu->pc_ =(cpu->pc_ +((int32_t)(imm)));
			}else{	
				cpu->pc_ = cpu->pc_ + 4;
			};
			break;
		//BNE	
		case 1:
			if(cpu->regfile_[rs1] != cpu->regfile_[rs2]){
				cpu->pc_ = cpu->pc_ + ((int32_t)(imm));
			}else{
				cpu->pc_ = cpu->pc_ + 4;
			}
			break;
		//BLT
		case 4:
			if(cpu->regfile_[rs1] < cpu->regfile_[rs2]){
				cpu->pc_ = cpu->pc_ + ((int32_t)(imm));
				
			}else{
				cpu->pc_ = cpu->pc_ + 4;
				
			}
			break;
		//BGE
		case 5:
			if((int32_t)cpu->regfile_[rs1] >= (int32_t)cpu->regfile_[rs2]){
				cpu->pc_ = cpu->pc_ + ((int32_t)(imm));
				
			}else{
				cpu->pc_ = cpu->pc_ + 4;	
			}
			break;
		//BLTU
		case 6:
			if(cpu->regfile_[rs1] < cpu->regfile_[rs2]){
				cpu->pc_ = cpu->pc_ + ((int32_t)(imm));
			}else{
				cpu->pc_ = cpu->pc_ + 4;
			}
			break;
		//BGEU
		case 7:
			if(cpu->regfile_[rs1] >= cpu->regfile_[rs2]){
				cpu->pc_ = cpu->pc_ + (int32_t)(imm);
				
			}else{
				cpu->pc_ = cpu->pc_ + 4;
			}
			break;
	}
}

void jal_handler(uint32_t instruction, CPU* cpu){
    uint8_t rd = extract_rd(instruction);
    uint32_t imm = extract_jal_imm(instruction);
    cpu->regfile_[rd] = cpu->pc_ +4;
    cpu->pc_ = cpu->pc_ + ((int32_t) imm);
}

void jalr_handler(uint32_t instruction, CPU* cpu){
    uint8_t rd = extract_rd(instruction);
    uint8_t rs1 = extract_rs1(instruction);
    uint32_t imm = extract_jalr_imm(instruction);
    cpu->regfile_[rd]= cpu->pc_ + 4;
    cpu->pc_ = (cpu ->regfile_[rs1]) + ((int32_t)imm);
}






void CPU_execute(CPU* cpu) {

	uint32_t instruction = *(uint32_t*)(cpu->instr_mem_ + (cpu->pc_  & 0xFFFFF));
	//printf("instruction %d\n", instruction);
	uint8_t opcode = (uint8_t)(0x7F &  instruction);
	//printf("%X\n", instruction);
	//printf("opcode %d\n", opcode);

	switch(opcode){
		case 3:
			//printf("Need L-Handler. \n");
			l_handler(instruction,cpu); //done
			break;
		case 19:
			//printf("Need I-Handler. \n");
			i_handler(instruction,cpu); // done
			break;
		case 23:
			//printf("Need AUIPC-Handler. \n");
			auipc_handler(instruction,cpu); //done
			break;
		case 35:
			//printf("Need S-Handler. \n");
			s_handler(instruction, cpu); //done
			break;
		case 51:
			//printf("Need R-Handler. \n");
			r_handler(instruction, cpu); //done
			break;
		case 55:
			//printf("Need LUI-Handler. - implemented \n");
			lui_handler(instruction,cpu); //done
			break;
		case 99:
			//printf("Need B-Handler. \n");
			b_handler(instruction,cpu); //done
			break;
		case 103:
			//printf("Need JALR-Handler. \n");
			jalr_handler(instruction,cpu); //done
			break;
		case 111:
			//printf("Need JAL-Handler. \n");
			jal_handler(instruction,cpu); //done
			break;
	}
	cpu->regfile_[0]=0;


}

int main(int argc, char* argv[]) {
	printf("C Praktikum\nHU Risc-V  Emulator 2022\n");

	CPU* cpu_inst;

	cpu_inst = CPU_init(argv[1], argv[2]);
	//1000000
    for(uint32_t i = 0; i < 1000000; i++) { // run 70000 cycles
    	CPU_execute(cpu_inst);
		
    }

	printf("\n-----------------------RISC-V program terminate------------------------\nRegfile values:\n");

	//output Regfile
	for(uint32_t i = 0; i <= 31; i++) {
    	printf("%d: %X\n",i,cpu_inst->regfile_[i]);
    }
    fflush(stdout);

	return 0;
}
